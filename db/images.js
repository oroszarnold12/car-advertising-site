const execute = require('./config');

execute(`CREATE TABLE IF NOT EXISTS images (
    cid varchar(36),
    image varchar(39),
    foreign key (cid) references cars(cid));`);

exports.deleteCarImages = (cid) => execute('DELETE FROM images WHERE images.cid = ?', [cid])
  .then((result) => result.affectedRows > 0);
