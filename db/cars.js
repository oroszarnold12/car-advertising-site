const execute = require('./config');

execute(`CREATE TABLE IF NOT EXISTS cars (
    cid varchar(36) primary key,
    manufacture varchar(50),
    model varchar(50),
    year int,
    city varchar(50),
    fuel varchar(50),
    price int,
    date varchar(50),
    uid int,
    foreign key (uid) references users(uid));`);

exports.findCarFuelAndDate = (cid) => execute('SELECT cars.fuel, cars.date FROM cars WHERE cars.cid = ?', [cid])
  .then((fuelAndDate) => fuelAndDate[0]);

exports.deleteCarImage = (cid, image) => execute('DELETE FROM images WHERE images.cid = ? AND images.image = ?', [cid, image])
  .then((result) => result.affectedRows > 0);

exports.findAllCars = () => execute('SELECT * FROM cars');

exports.findCarsByFuel = (fuel) => execute('SELECT * FROM cars WHERE cars.fuel = ?', [fuel]);

exports.findCarById = (cid) => execute('SELECT * FROM cars WHERE cars.cid = ?', [cid])
  .then((cars) => cars[0]);

exports.insertCar = (car, uid) => execute('INSERT INTO cars VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
  [car.cid, car.manufacture, car.model, car.year, car.city, car.fuel,
    car.price, car.date, uid])
  .then(() => ({ cid: car.cid, manufacture: car.manufacture, model: car.model }));

exports.deleteCar = (cid) => execute('DELETE FROM cars WHERE cars.cid = ?', [cid])
  .then((result) => result.affectedRows > 0);

exports.updateCar = (cid, car) => execute('UPDATE cars SET cars.price = ? WHERE cars.cid = ?', [car.price, cid])
  .then((result) => result.affectedRows > 0);
