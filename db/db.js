const mysql = require('mysql'),
  uuid = require('uuid');

const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

pool.query(`CREATE TABLE IF NOT EXISTS users (
  uid int auto_increment,
  uname varchar(50),
  upassword varchar(96),
  urole varchar(20),
  primary key(uid));`, (err) => {
  if (err) {
    console.error(`Create table error: ${err.message}`);
    process.exit(1);
  } else {
    console.log('Table created successfully');
  }
});

pool.query(`CREATE TABLE IF NOT EXISTS cars (
    cid varchar(36) primary key,
    manufacture varchar(50),
    model varchar(50),
    year int,
    city varchar(50),
    fuel varchar(50),
    price int,
    date varchar(50),
    uid int,
    foreign key (uid) references users(uid));`, (err) => {
  if (err) {
    console.error(`Create table error: ${err.message}`);
    process.exit(1);
  } else {
    console.log('Table created successfully');
  }
});

pool.query(`CREATE TABLE IF NOT EXISTS images (
    cid varchar(36),
    image varchar(39),
    foreign key (cid) references cars(cid));`, (err) => {
  if (err) {
    console.error(`Create table error: ${err.message}`);
    process.exit(1);
  } else {
    console.log('Table created successfully');
  }
});

exports.insertUploadedImages = (req, callback) => {
  const files = req.files.myfile;
  if (Array.isArray(files)) {
    for (let j = 0; j < files.length; j += 1) {
      const query = `INSERT INTO images VALUES("${req.fields.cid}", "${files[j].path.substring(files[j].path.lastIndexOf('\\') + 1)}")`;
      console.log(query);
      pool.query(query, (err) => {
        if (j === files.length - 1) {
          callback(err);
        }
      });
    }
  } else {
    const query = `INSERT INTO fenykep VALUES("${req.fields.cid}", "${files.path.substring(files.path.lastIndexOf('\\') + 1)}")`;
    console.log(query);
    pool.query(query, callback);
  }
};

exports.insertCarInformations = (req, userId, callback) => {
  const genid = uuid.v4();
  const date = new Date().toISOString();
  const query = `INSERT INTO cars VALUES ("${genid}", "${req.fields.manufacture}", "${req.fields.model}", "${req.fields.year}",
                    "${req.fields.city}", "${req.fields.fuel}", "${req.fields.price}", "${date}", "${userId}");`;
  console.log(query);
  pool.query(query, callback);
};

exports.findAllCar = (callback) => {
  const query = 'SELECT * FROM cars';
  console.log(query);
  pool.query(query, callback);
};

exports.findFilteredCars = (manufacture, city, minprice, maxprice, callback) => {
  let finalManufacture = manufacture,
    finalCity = city,
    finalMinPrice = minprice,
    finalMaxPrice = maxprice;
  if (finalManufacture.length === 0) {
    finalManufacture = ' *';
  }
  if (finalCity.length === 0) {
    finalCity = ' *';
  }
  if (finalMinPrice.length === 0) {
    finalMinPrice = 0;
  }
  if (finalMaxPrice.length === 0) {
    finalMaxPrice = 9999999999;
  }
  const query = `SELECT * FROM cars WHERE cars.manufacture RLIKE "${finalManufacture}" AND cars.city RLIKE "${finalCity}" AND 
                cars.price >= "${finalMinPrice}" AND cars.price <= "${finalMaxPrice}"`;
  console.log(query);
  pool.query(query, callback);
};

exports.findCarImages = (cid, callback) => {
  const query = `SELECT image FROM images RIGHT JOIN cars ON images.cid = cars.cid WHERE images.cid = "${cid}"`;
  console.log(query);
  pool.query(query, callback);
};

exports.findRespectiveCar = (cid, callback) => {
  const query = `SELECT * FROM cars WHERE cars.cid = "${cid}"`;
  console.log(query);
  pool.query(query, callback);
};

exports.getUserPasswordRoleId = (userName, callback) => {
  const query = `SELECT users.uid, users.upassword, users.urole FROM users WHERE users.uname = "${userName}"`;
  console.log(query);
  pool.query(query, callback);
};

exports.deleteCarImages = (cid, callback) => {
  const query = `DELETE FROM images WHERE images.cid = "${cid}"`;
  console.log(query);
  pool.query(query, callback);
};

exports.deleteRespectiveCar = (cid, callback) => {
  const query = `DELETE FROM cars WHERE cars.cid = "${cid}"`;
  console.log(query);
  pool.query(query, callback);
};

exports.getCarUserId = (cid, callback) => {
  const query = `SELECT cars.uid FROM cars WHERE cars.cid = "${cid}"`;
  console.log(query);
  pool.query(query, callback);
};
