const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

module.exports = (query, options = []) => new Promise((resolve, reject) => {
  pool.query(query, options, (error, results) => {
    if (error) {
      reject(new Error(`Error while running '${query}: ${error}'`));
    }
    resolve(results);
  });
});
