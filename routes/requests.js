const express = require('express'),
  fs = require('fs'),
  db = require('../db/db');

const router = express.Router();

const regForCityAndManifacture = /^[A-Z][a-z]+$/;

function testCityOrManifacture(string) {
  return regForCityAndManifacture.test(string) || string.length === 0;
}

function testPriceOrYear(number) {
  if (number.length !== 0) {
    return number >= 0;
  }
  return true;
}

router.get(['/', '/index'], (req, res) => {
  db.findAllCar((err, cars) => {
    if (err) {
      res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}` });
    } else {
      console.log('Selection successful');
      const user = req.query.jwtDecoded;
      res.render('ad', { cars, user });
    }
  });
});

router.get('/get_respective_cars', (req, res) => {
  let respBody = '';
  if (!testCityOrManifacture(req.query.manufacture)) {
    respBody += 'Hibás márkát adott meg!\n';
  }
  if (!testCityOrManifacture(req.query.city)) {
    respBody += 'Hibás várost adott meg!\n';
  }
  if (!testPriceOrYear(req.query.minprice)) {
    respBody += 'Hibás árat adott meg!\n';
  }
  if (!testPriceOrYear(req.query.maxprice)) {
    respBody += 'Hibás árat adott meg!\n';
  }
  if (respBody.length === 0) {
    db.findFilteredCars(req.query.manufacture, req.query.city, req.query.minprice,
      req.query.maxprice, (err, cars) => {
        if (err) {
          res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}` });
        } else {
          console.log('Selection successful');
          const user = req.query.jwtDecoded;
          res.render('ad', { cars, user });
        }
      });
  } else {
    res.status(500).render('error', { message: `${respBody}` });
  }
});

router.get('/car_details', (req, res) => {
  db.findRespectiveCar(req.query.cid, (errcar, selectedCar) => {
    if (errcar) {
      res.status(500).render('error', { message: `Selection unsuccessful: ${errcar.message}` });
    } else {
      console.log('Selection successful');
      db.findCarImages(selectedCar[0].cid, (errimages, images) => {
        if (errimages) {
          res.status(500).render('error', { message: `Selection unsuccessful: ${errimages.message}` });
        } else {
          console.log('Selection successful');
          const car = selectedCar[0];
          car.image = images;
          const user = req.query.jwtDecoded;
          res.render('adDetails', { car, user });
        }
      });
    }
  });
});

router.get('/deletecookie', (req, res) => {
  console.log('Deleting cookie');
  res.clearCookie('token');
  res.redirect('/');
});

router.get('/uploadCarInf', (req, res) => {
  const user = req.query.jwtDecoded;
  res.render('uploadCarInf', { user });
});

router.get('/car_delete', (req, res, next) => {
  db.findCarImages(req.query.cid, (err, images) => {
    if (err) {
      res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}` });
    } else {
      console.log('Selection successful');
      let done = true;
      for (let i = 0; i < images.length; i += 1) {
        try {
          fs.unlinkSync(`./uploadDir/${images[i].image}`);
        } catch (errunlink) {
          res.status(500).render('error', { message: `Unlink unsuccessful: ${errunlink.message}` });
          done = false;
          break;
        }
      }
      if (done) {
        console.log('Unlink successful');
        next();
      }
    }
  });
});

router.get('/car_delete', (req, res) => {
  db.deleteCarImages(req.query.cid, (errImages) => {
    if (errImages) {
      res.status(500).render('error', { message: `Delete unsuccessful: ${errImages.message}` });
    } else {
      console.log('Image Delete successful');
      db.deleteRespectiveCar(req.query.cid, (errCar) => {
        if (errCar) {
          res.status(500).render('error', { message: `Delete unsuccessful: ${errCar.message}` });
        } else {
          console.log('Car Delete successful');
          res.redirect('/');
        }
      });
    }
  });
});

module.exports = router;
