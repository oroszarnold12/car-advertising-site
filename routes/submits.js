const express = require('express'),
  crypto = require('crypto'),
  jwt = require('jsonwebtoken'),
  db = require('../db/db');

const hashSize = 32,
  hashAlgorithm = 'sha512',
  iterations = 1000;

const secret = '1c28d07215544bd1b24faccad6c14a04';

const router = express.Router();

const regForCityAndManifacture = /^[A-Z][a-z]+$/;
const regForModel = /^[A-Z][a-z0-9]+$/;
const regForFuel = /^[a-z]+$/;

function testCityOrManifacture(string) {
  return regForCityAndManifacture.test(string) && string.length !== 0;
}

function testModel(string) {
  return regForModel.test(string) && string.length !== 0;
}

function testFuel(string) {
  return regForFuel.test(string) && string.length !== 0;
}

function testPriceOrYear(number) {
  if (number.length !== 0) {
    return number >= 0;
  }
  return false;
}

router.post('/upload_car_inf', (req, res) => {
  let respBody = '';
  if (!testCityOrManifacture(req.fields.manufacture)) {
    respBody += 'Wrong manufacture!\n';
  }
  if (!testModel(req.fields.model)) {
    respBody += 'Wrong model!\n';
  }
  if (!testPriceOrYear(req.fields.year)) {
    respBody += 'Wrong year!\n';
  }
  if (!testCityOrManifacture(req.fields.city)) {
    respBody += 'Wrong city!\n';
  }
  if (!testPriceOrYear(req.fields.price)) {
    respBody += 'Wrong price!\n';
  }
  if (!testFuel(req.fields.fuel)) {
    respBody += 'Wrong fuel!\n';
  }
  if (respBody.length === 0) {
    const userId = req.query.jwtDecoded.id;
    db.insertCarInformations(req, userId, (err) => {
      if (err) {
        res.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}` });
      } else {
        console.log('Insertion successful');
        res.redirect('/');
      }
    });
  } else {
    res.status(500).render('error', { message: `${respBody}` });
  }
});

router.post('/upload_car_image', (req, res) => {
  db.insertUploadedImages(req, (err) => {
    if (err) {
      res.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}` });
    } else {
      console.log('Insertion successful');
      res.redirect(`/car_details?cid=${req.fields.cid}`);
    }
  });
});

router.post('/login', (req, res) => {
  db.getUserPasswordRoleId(req.fields.userName, (err, passwordRoleId) => {
    if (err) {
      res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}` });
    } else {
      console.log('Selecton succesful');
      if (passwordRoleId.length !== 0) {
        const expectedHash = passwordRoleId[0].upassword.substring(0, hashSize * 2),
          salt = Buffer.from(passwordRoleId[0].upassword.substring(hashSize * 2), 'hex');
        crypto.pbkdf2(req.fields.password, salt, iterations, hashSize, hashAlgorithm,
          (passCheckErr, binaryHash) => {
            if (passCheckErr) {
              res.status(500).render('error', { message: `Hashing unsuccessful: ${passCheckErr.message}` });
            } else {
              console.log('Hashing succesful');
              const actualHash = binaryHash.toString('hex');
              if (expectedHash === actualHash) {
                console.log('Passwords match');
                const id = passwordRoleId[0].uid;
                const name = req.fields.userName;
                const role = passwordRoleId[0].urole;
                const token = jwt.sign({ name, role, id }, secret);
                console.log(token);
                res.cookie('token', token);
                res.redirect('/');
              } else {
                res.status(401).render('error', { message: 'Passwords do not match' });
              }
            }
          });
      } else {
        res.status(401).render('error', { message: 'This user does not exists' });
      }
    }
  });
});

module.exports = router;
