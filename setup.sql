-- Segédállomány, hogy előkészítsünk egy MySQL adatbázist a példaprogramnak.
-- Futtatás konzolról (UNIX rendszeren): 
--     mysql -u root -p <setup.sql

-- az alábbi sor törli az adatbázist ha létezik
DROP DATABASE IF EXISTS webprog;

-- készít egy adatbázist
CREATE DATABASE IF NOT EXISTS webprog;

-- készít egy felhasználót, aki minden műveletet végrehajthat ezen adatbázisban
USE webprog;
GRANT ALL PRIVILEGES ON *.* TO 'webprog'@'localhost' IDENTIFIED BY 'VgJUjBd8';

-- készít egy felhasznalo tablat a weboldalhoz
CREATE TABLE IF NOT EXISTS users (
  uid int auto_increment,
  uname varchar(50),
  upassword varchar(96),
  urole varchar(20),
  primary key(uid));

-- készít egy standard és egy admin usert
-- username: user, password: user
-- username: admin, password: admin
insert into users(uname, upassword, urole) values ('admin', '0015017947cd1fbbb0a035f83b60d639f9f027e1bab0834df6741c6acf1fe58ec32b7052dcd47c3c137cf8688ad6fd1c', 'admin'),
('user', '6414181767dbb51a666c81617fc6c2c04b524052764b9479ac464fe9f3adf8a615d918321fe726c36541f30daea3cbb5', 'user');
