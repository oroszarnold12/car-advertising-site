const express = require('express'),
  path = require('path'),
  morgan = require('morgan'),
  fs = require('fs'),
  cookieParser = require('cookie-parser'),
  ehandlebars = require('express-handlebars'),
  eformidable = require('express-formidable'),
  helpers = require('handlebars-helpers'),
  requestRoutes = require('./routes/requests'),
  submitRoutes = require('./routes/submits'),
  errorMiddleware = require('./middleware/error'),
  authorizeMiddleware = require('./middleware/authorize'),
  authorizeapiMiddleware = require('./middleware/authorizeapi'),
  jwtDecode = require('./middleware/jwtDecode'),
  apiRoutes = require('./api');

helpers(ehandlebars);

const staticDir = path.join(__dirname, 'static');
const uploadDir = path.join(__dirname, 'uploadDir');

if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

const app = express();

app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));
app.engine('hbs', ehandlebars({
  extname: 'hbs',
  defaultView: 'main',
  layoutsDir: path.join(__dirname, 'views/layouts'),
  partialsDir: path.join(__dirname, 'views/partials'),
}));

app.use(morgan('tiny'));
app.use(cookieParser());

app.use(jwtDecode);

app.use('/api/cars/', authorizeapiMiddleware());
app.use('/api/cars/:cid/', authorizeapiMiddleware());
app.use('/api/cars/:cid/images/:image', authorizeapiMiddleware());

app.use('/api', apiRoutes);

app.use(eformidable({ uploadDir, multiples: true }));
app.use(['/upload_car_image', '/upload_car_inf'], authorizeMiddleware());
app.use('/car_delete', authorizeMiddleware(['admin']));

app.use(submitRoutes);

app.use(express.urlencoded({ extended: true }));
app.use(requestRoutes);

app.use(express.static(staticDir));
app.use(express.static(uploadDir));

app.use(errorMiddleware);

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
