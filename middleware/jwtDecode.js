const jwtDecode = require('jwt-decode');

module.exports = (req, res, next) => {
  if (req.cookies.token) {
    req.query.jwtDecoded = jwtDecode(req.cookies.token);
  }
  next();
};
