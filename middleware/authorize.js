const jwt = require('jsonwebtoken'),
  db = require('../db/db');

const secret = '1c28d07215544bd1b24faccad6c14a04';

module.exports = (roles = ['user', 'admin']) => (req, res, next) => {
  if (req.cookies.token) {
    const { token } = req.cookies;
    jwt.verify(token, secret, (err, payload) => {
      if (payload) {
        console.log(payload);
        if (roles.includes(payload.role)) {
          if (req.fields.cid) {
            db.getCarUserId(req.fields.cid, (errId, userId) => {
              if (errId) {
                res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}` });
              } else if (userId[0].uid === payload.id) {
                console.log('Selection successful');
                console.log(`JWT successfully verified for ${payload.name}`);
                next();
              } else {
                res.status(401).render('error', { message: 'You do not have permission to access this endpoint' });
              }
            });
          } else {
            console.log(`JWT successfully verified for ${payload.name}`);
            next();
          }
        } else {
          res.status(401).render('error', { message: 'You do not have permission to access this endpoint' });
        }
      } else {
        res.status(401).render('error', { message: 'You are not logged in properly' });
      }
    });
  } else {
    res.status(401).render('error', { message: 'You are not logged in properly' });
  }
};
