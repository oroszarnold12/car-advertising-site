function adClicked(cid) { // eslint-disable-line no-unused-vars
  const table = document.getElementById(cid);
  const fuelRowIndex = 5,
    dateRowIndex = 6,
    labelIndex = 0,
    contentIndex = 1,
    errorRowIndex = 7,
    errorContentIndex = 0;
  fetch(`/api/cars/${cid}/extra`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((res) => {
      if (res.status !== 200) {
        throw new Error('We have encountered an error');
      }
      return res;
    })
    .then((res) => res.json())
    .then((res) => {
      table.rows[fuelRowIndex].cells[labelIndex].innerText = 'Fuel:';
      table.rows[fuelRowIndex].cells[contentIndex].innerText = res.fuel;
      table.rows[dateRowIndex].cells[labelIndex].innerText = 'Date:';
      table.rows[dateRowIndex].cells[contentIndex].innerText = res.date;
    })
    .catch((err) => {
      table.rows[errorRowIndex].cells[errorContentIndex].innerText = err.message;
    });
}
