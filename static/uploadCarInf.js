function clickValidate() { // eslint-disable-line no-unused-vars
  const manifacture = document.getElementById('manufacture');
  const model = document.getElementById('model');
  const year = document.getElementById('year');
  const city = document.getElementById('city');
  const fuel = document.getElementById('fuel');
  const price = document.getElementById('price');
  const form = document.getElementById('form_id');
  const msg = document.getElementById('msg');
  const submit = document.getElementById('submit');
  if (!form.checkValidity()) {
    msg.innerHTML = 'Invalid data!';
    return;
  }
  if (String(manifacture.value).length === 0) {
    msg.innerHTML = 'Manufacture required!';
    return;
  }
  if (String(model.value).length === 0) {
    msg.innerHTML = 'Model required!';
    return;
  }
  if (String(year.value).length === 0) {
    msg.innerHTML = 'Year required!';
    return;
  }
  if (String(city.value).length === 0) {
    msg.innerHTML = 'City required!';
    return;
  }
  if (String(fuel.value).length === 0) {
    msg.innerHTML = 'Fuel required!';
    return;
  }
  if (String(price.value).length === 0) {
    msg.innerHTML = 'Price required!';
    return;
  }
  msg.innerHTML = '';
  submit.disabled = false;
}
