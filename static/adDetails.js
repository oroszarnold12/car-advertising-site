function deleteImage(element, image, cid) { // eslint-disable-line no-unused-vars
  const rowNumber = element.parentNode.parentNode.rowIndex;
  const table = document.getElementById('images');
  const errorContentIndex = 2;
  fetch(`/api/cars/${cid}/images/${image}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((res) => {
      if (res.status !== 200) {
        throw new Error('We have encountered an error');
      }
      return res;
    })
    .then((res) => res.json())
    .then((res) => {
      table.rows[rowNumber].innerText = res.message;
    })
    .catch((err) => { table.rows[rowNumber].cells[errorContentIndex].innerText = err.message; });
}
