const express = require('express'),
  fs = require('fs'),
  uuid = require('uuid'),
  carDao = require('../db/cars'),
  imageDao = require('../db/images'),
  validate = require('../middleware/validate');

const router = express.Router();

router.get('/:cid/extra', (req, res) => {
  const { cid } = req.params;
  carDao.findCarFuelAndDate(cid)
    .then((fuelAndDate) => (fuelAndDate ? res.json(fuelAndDate) : res.status(404).json({ message: `Car's fuel and date with ID ${cid} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding car's fuel and date with ID ${cid}: ${err.message}` }));
});

router.delete('/:cid/images/:image', (req, res) => {
  const { cid, image } = req.params;
  carDao.deleteCarImage(cid, image)
    .then((rows) => {
      if (rows) {
        fs.unlink(`./uploadDir/${image}`, (errunlink) => {
          if (errunlink) {
            res.status(500).json({ message: `Error deleting car's with ID ${cid} image ${image}: ${errunlink.message}` });
          } else {
            res.json({ message: 'Image removed' });
          }
        });
      } else {
        res.status(404).json({ message: `Car's image ${image} with ID ${cid} not found` });
      }
    })
    .catch((err) => res.status(500).json({ message: `Error deleting car's with ID ${cid} image ${image}: ${err.message}` }));
});

router.get('/', (req, res) => {
  if (req.query.fuel) {
    carDao.findCarsByFuel(req.query.fuel)
      .then((cars) => res.json(cars))
      .catch((err) => res.status(500).json({ message: `Error while finding cars by fuel: ${err.message}` }));
  } else {
    carDao.findAllCars()
      .then((cars) => res.json(cars))
      .catch((err) => res.status(500).json({ message: `Error while finding all cars: ${err.message}` }));
  }
});

router.get('/:cid', (req, res) => {
  const { cid } = req.params;
  carDao.findCarById(cid)
    .then((car) => (car ? res.json(car) : res.status(404).json({ message: `Car with ID ${cid} not found` })))
    .catch((err) => res.status(500).json({ message: `Error while finding car with ID ${cid}: ${err.message}` }));
});

router.post('/', validate.hasProps(['manufacture', 'model', 'year', 'city', 'fuel', 'price']), (req, res) => {
  req.body.cid = uuid.v4();
  req.body.date = new Date().toISOString();
  carDao.insertCar(req.body, req.query.jwtDecoded.id)
    .then((car) => res.status(201).location(`${req.fullUrl}/{${car.cid}}`).json(car))
    .catch((err) => res.status(500).json({ message: `Error while creating car: ${err.message}` }));
});

router.delete('/:cid', (req, res) => {
  const { cid } = req.params;
  imageDao.deleteCarImages(cid)
    .then(() => carDao.deleteCar(cid))
    .then((rows) => (rows ? res.sendStatus(204) : res.status(404).json({ message: `Car with ID: ${cid} not found` })))
    .catch((err) => res.status(500).json({ message: `Error while deleting car with ID: ${cid}: ${err.message}` }));
});

router.patch('/:cid', (req, res) => {
  const { cid } = req.params;
  carDao.updateCar(cid, req.body)
    .then((rows) => (rows ? res.sendStatus(204) : res.status(404).json({ message: `Car with ID ${cid} not found` })))
    .catch((err) => res.status(500).json({ message: `Error while updating car with ID ${cid}: ${err.message}` }));
});

module.exports = router;
