const express = require('express'),
  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  carRoutes = require('./cars'),
  fullUrlMiddleware = require('../middleware/fullurl');

const router = express.Router();

router.use(morgan('tiny'));

router.use(fullUrlMiddleware);

router.use(bodyParser.json());

router.use('/cars', carRoutes);

module.exports = router;
